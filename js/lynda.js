var dataArray = [5,11,18];

var svg = d3.select("body").append("svg").attr("class", "wrapper");

svg.selectAll("rect")
	.data(dataArray)
	.enter().append("rect")
	.attr("height",function(d) {return d*15;})
	.attr("width","50")
	.attr("fill", "red")
	.attr("x",function(d,i) {return 60*i;})
	.attr("y",function(d,i) {return 300-d*15;});
	
	var newX = 300;
	svg.selectAll("circle.first")
	.data(dataArray)
	.enter().append("circle")
	.attr("class","first")
	.attr("fill", "green")
	.attr("cx",function(d,i) { newX+=d*6+i*20; return newX;})
	.attr("cy","100")
	.attr("r",function(d) { return d*3;});
	
	var newX = 600;
	svg.selectAll("ellipse.second")
	.data(dataArray)
	.enter().append("ellipse")
	.attr("class", "second")
	.attr("fill", "green")
	.attr("cx",function(d,i) { newX+=d*3+i*20; return newX;})
	.attr("cy","100")
	.attr("rx",function(d) { return d*3;})
	.attr("ry", "30");
	