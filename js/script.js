var data = ["Station"];
var stations1 = {};
var stations2 = {};
var connections;
var full1 = true;
var full2 = false;
var old_one = 1;

var chart = bb.generate({
    bindto: "#chart",
	size: {
		height: 460
	},
	data: {
		x: "x",
        xFormat: "%Y-%m-%dT%H:%M:%S%Z",
        columns: [
			["x"],
			data
		],
		axes: {
			Station: "y",
			Station2: "y2"
		}
	},
	tooltip: {
		grouped: false
	},
	axis: {
		x: {
			type: "timeseries",
			tick: {
				format: "%H:%M",
				outer: false,
				culling: false,
			}
		},
		y: {
			tick: {
				fit: true,
				outer: false
			}
		},
		y2: {
			show: true,
			tick: {
				fit: true
			}
		}
	},
	grid: {
		y: {
			show: true
		},
		y2: {
			show: true
		}
	},
	legend: {
		show: false
	},
	zoom: {
		enabled: true
	}
});

	
function searchConnections(form) {
document.getElementById("loader").setAttribute("class","active");
if(form.von.value != "" && form.bis.value != "") {
var von = form.von.value;
var bis = form.bis.value;
var limit = 4;
var time = new Date();
var offset = time.getTimezoneOffset()/60;
time.setHours(time.getHours() - offset);
console.log(time.toISOString());
	
var url = "http://transport.opendata.ch/v1/connections?from=" + von + "&to=" + bis + "&limit=" + limit + "&datetime=" + time.toISOString();
	
d3.json(url, function(d) {
document.getElementById("loader").classList.remove("active");
console.log(d.connections);
if(d.connections.length == 0) {
	alert("Es wurde keine Verbindung gefunden!");
} else {
connections = d.connections;
ond_one = 1;
chart.unload();
chart.flush();

stations1 = {};
stations2 = {};

var text = "";
for(i=0;i<limit;i++) {
	var c = connections[i];
	var departure = new Date(c.from.departure);
	var departureTime = departure.toTimeString().split(' ')[0];
	var arrival = new Date(c.to.arrival);
	var arrivalTime = arrival.toTimeString().split(' ')[0];
	var duration = c.duration.split('d')[1];
	
text += "<div id=\"connection" + i + "\" class=\"connections_overview\" onclick=\"select_connection(" + i + ")\">";
text += "<p>" + departureTime + " - " + arrivalTime + " | Richtung " + c.sections[0].journey.to + "</br>Dauer: " + duration + " | " + c.transfers + " Mal Umsteigen</p>";
text += "</div>";
}

document.getElementById("overview").innerHTML = text;
select_connection(0);
}

});
	
} else {
	alert("Nicht alle Felder sind ausgefuellt!");
}
	return false;
}
	
	
var select_connection = function(index) {
var selected = document.getElementById("connection" + index);
console.log("select_connection(" + index + ") aufgerufen!");
var sections = connections[index].sections;
timeUnits = ["x"];
if(old_one == 1) {
	data = ["Station"];
	if(document.getElementsByClassName("x1")[0]) {
		document.getElementsByClassName("x1")[0].classList.remove("x1");
	}
	selected.classList.add("x1");
} else if(old_one == 2) {
	data = ["Station2"];
	if(document.getElementsByClassName("x2")[0]) {
		document.getElementsByClassName("x2")[0].classList.remove("x2");
	}
	selected.classList.add("x2");
}
stations = {};

for(i=0;i<sections.length;i++) {
	
	// Datum und Zeit von der Abfahrt wird im Array timeUnits gespeichert
	timeUnits.push(sections[i].departure.departure);
	// Datum und Zeit von der Ankunft wird im Array timeUnits gespeichert
	timeUnits.push(sections[i].arrival.arrival);
	if(i==0) {
		// Speichert die Abfahrtszeit als Timestamp in das Array data
		data.push(sections[i].departure.departureTimestamp);
		// Fügt den Namen der Station in das assoziative Array stations ein. (key = Abfahrtszeit als Timestamp, value = Name der Station)
		stations[sections[i].departure.departureTimestamp.toString()] = sections[i].departure.location.name;
	} else {
		// Speichert die Ankunftszeit der vorherigen Verbindung als Timestamp in das Array data
		data.push(sections[i-1].arrival.arrivalTimestamp);
	}
	
	if(sections[i].journey == null && sections[i].walk.duration > 0) {
		stations[sections[i].departure.departureTimestamp.toString()] = sections[i].departure.location.name;
	}
	
	data.push(sections[i].arrival.arrivalTimestamp);
	stations[sections[i].arrival.arrivalTimestamp.toString()] = sections[i].arrival.location.name;

}

console.log(timeUnits);
console.log(data);
console.log(stations);

if(old_one == 1) {
	stations1 = stations;
	chart.internal.config.axis_y_tick_values = data;
	chart.internal.config.axis_y_tick_format = function (d) {
		return stations1[d];
	};
	chart.flush();
	
	chart.load({
	columns: [
		timeUnits,
		data
    ],
	unload: "Station"
});
	old_one = 2;
} else if(old_one == 2) {
	stations2 = stations;
	chart.internal.config.axis_y2_tick_values = data;
	chart.internal.config.axis_y2_tick_format = function (d) {
		return stations2[d];
	};
	chart.flush();
	
	chart.load({
	columns: [
		timeUnits,
		data
    ],
	unload: "Station2"
});
old_one = 1;
}


}
